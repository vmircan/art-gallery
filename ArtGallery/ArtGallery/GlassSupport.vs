#version 330

layout (location = 0) in vec3 coordv;
layout (location = 1) in vec3 normalv;

out vec3 normal;
out vec3 pos;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main() {
	gl_Position =  projection * view * model * vec4(coordv, 1.0);
	normal = mat3(transpose(inverse(model))) * normalv;
	pos = vec3(model * vec4(coordv, 1.0));
}