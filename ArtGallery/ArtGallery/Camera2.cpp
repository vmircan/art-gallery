#include "Camera2.h"
#include "gtc\matrix_transform.hpp"
#include <iostream>


Camera2::Camera2(float x, float y, float z, float upX,
	float upY, float upZ, float fov, float yaw, float pitch, float roll) :
	fov(fov), position(x, y, z), up(upX, upY, upZ), yaw(yaw), pitch(pitch), roll(roll),
	initialPosition(position), initialUp(up), initialFov(fov), initialYaw(yaw), initialPitch(pitch), initialRoll(roll)
{
	refreshVectors();
}

Camera2::~Camera2()
{
}

void Camera2::refreshVectors()
{
	glm::vec3 temp = glm::normalize(glm::vec3(cos(glm::radians(yaw)) * cos(glm::radians(pitch)), 
		sin(glm::radians(pitch)), sin(glm::radians(yaw)) * cos(glm::radians(pitch))));
	front = glm::normalize(temp);
	right = glm::normalize(glm::cross(front, initialUp));
	up = glm::normalize(glm::cross(right, front));
}

void Camera2::updateZoomLevel(double zoom)
{
	zoom *= mouseScrollSensitivity;
	fov -= static_cast<float>(zoom);
	if (fov < minFov)
		fov = minFov;
	else if (fov > maxFov)
		fov = maxFov;
}

void Camera2::processMouseMovement(double x, double y)
{
	if (firstMouse)
	{
		firstMouse = false;
	}

	float xoffset = static_cast<float>(x) - centerX;
	float yoffset = centerY - static_cast<float>(y);

	float sensitivity = 0.05f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	else if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	this->front = glm::normalize(front);
}

void Camera2::processKeyboardInput(int key, int action)
{
	switch (key)
	{
	case GLFW_KEY_UP:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		position += front*keySensitivity;
		refreshVectors();
		break;
	case GLFW_KEY_DOWN:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		position -= front*keySensitivity;
		refreshVectors();
		break;
	case GLFW_KEY_LEFT:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		position -= right*keySensitivity;
		refreshVectors();
		break;
	case GLFW_KEY_RIGHT:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		position += right*keySensitivity;
		refreshVectors();
		break;
	case GLFW_KEY_PAGE_UP:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		position += up*keySensitivity;
		refreshVectors();
		break;
	case GLFW_KEY_PAGE_DOWN:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		position -= up*keySensitivity;
		refreshVectors();
		break;
	case GLFW_KEY_COMMA:
		if (!rotationEnabled) {
			rotationStartingPosition = position;
			rotationRadius = sqrt((position.x - rotationCenter.x) * (position.x - rotationCenter.x) +
				(position.y - rotationCenter.y) * (position.y - rotationCenter.y) +
				(position.z - rotationCenter.z) * (position.z - rotationCenter.z));
			front = rotationCenter;

			rotationEnabled = true;
		}
		yaw +=  keySensitivity;
		position.x = rotationCenter.x + sin(yaw) * rotationRadius;
		position.z = rotationCenter.z + cos(yaw) * rotationRadius;
		break;
	case GLFW_KEY_PERIOD:
		if (!rotationEnabled) {
			rotationStartingPosition = position;
			rotationRadius = sqrt((position.x - rotationCenter.x) * (position.x - rotationCenter.x) +
				(position.y - rotationCenter.y) * (position.y - rotationCenter.y) +
				(position.z - rotationCenter.z) * (position.z - rotationCenter.z));
			front = rotationCenter;
			rotationEnabled = true;
		}
		yaw -= keySensitivity;
		position.x = rotationCenter.x + sin(yaw) * rotationRadius;
		position.z = rotationCenter.z + cos(yaw) * rotationRadius;
		break;

	case GLFW_KEY_SEMICOLON:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		if(pitch < 89.0f)
			pitch += keySensitivity * 10;
		refreshVectors();
		break;
	case GLFW_KEY_APOSTROPHE:
		if (rotationEnabled) {
			rotationEnabled = false;
		}
		if(pitch > -44.0f)
			pitch -= keySensitivity * 10;
		refreshVectors();
		break;

	case GLFW_KEY_X:
		resetCamera();
		refreshVectors();
		rotationEnabled = false;
		break;
	default:
		break;
	}
	//refreshVectors();
}

const glm::vec3 & Camera2::getPosition() const
{
	return position;
}

const glm::vec3 & Camera2::getFront() const
{
	return front;
}

const glm::vec3 & Camera2::getUp() const
{
	return up;
}

const glm::mat4 Camera2::getViewMatrix() const
{
	if (!rotationEnabled)
		return glm::lookAt(position, front + position, up);
	else
		return glm::lookAt(position, rotationCenter, up);
}

void Camera2::toggleMouseMovement()
{
	mouseMovementEnabled = !mouseMovementEnabled;
}
float Camera2::getFov() const
{
	return fov;
}

float Camera2::getRotationRadius() const
{
	return rotationRadius;
}

void Camera2::setRotationRadius(float rotationRadius)
{
	this->rotationRadius = rotationRadius;
}

void Camera2::setRotationCenter(const glm::vec3 & rotationCenter)
{
	this->rotationCenter = rotationCenter;
}

void Camera2::setRotationCenter(double x, double y, double z)
{
	rotationCenter = glm::vec3(x, y, z);
}

void Camera2::setMouseMovementEnabled(bool enabled)
{
	mouseMovementEnabled = enabled;
}

bool Camera2::getMouseMovementEnabled() const
{
	return mouseMovementEnabled;
}

void Camera2::resetCamera()
{
	position = initialPosition;
	up = initialUp;
	fov = initialFov;
	yaw = initialYaw;
	pitch = initialPitch;
	roll = initialRoll;
	refreshVectors();
}