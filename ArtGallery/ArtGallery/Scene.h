#pragma once
#include <chrono>

#include "Camera2.h"
#include "OpenGLObject.h"
#include <unordered_map>

class Scene
{
public:
	static void addObject(const std::string& label, OpenGLObject* obj);
	static void initialize();
	static void update();
	static void draw();
	static void addCamera(Camera2* camera);
	static Camera2* getActiveCamera();

	static void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void mouseScrollCallback(GLFWwindow* window, double x, double y);
	static void mouseCallback(GLFWwindow* window, double x, double y);
	static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
private:
	static long long getTimeSinceLastToggle();

private:
	static bool wireFrame;
	const static unsigned toggleActivationDelay = 100;
	static long long lastToggleTime;
	static std::vector<Camera2*> cameras;
	static Camera2* activeCamera;
	static std::unordered_map<std::string, OpenGLObject*> objects;
};

