#pragma once
#include "Texture.h"
#include "Vertex.h"

#include <vector>
#include "ShaderLoader.h"

class Mesh {
public:
	Mesh(std::vector<Vertex> vertices, std::vector<unsigned> indices,
		std::vector<Texture> textures);
	void Draw(ShaderLoader shader);
	void addTexture(const Texture& texture);
private:
	std::vector<Vertex> vertices;
	std::vector<unsigned> indices;
	std::vector<Texture> textures;
	unsigned int VAO, VBO, EBO;
	void setupMesh();
};
