#include "Background.h"
#include "gtc\type_ptr.hpp"
#include "gtc\matrix_transform.hpp"

Background::Background(ShaderLoader shader):
	shader(shader)
{
}

Background::~Background()
{
}

void Background::initialize()
{
	initializeVertices();
	initializeBuffers();
}

void Background::initializeVertices()
{
	points = {
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,-1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
	};
	textureCoordinates = {
	    -1.0, -1.0, -1.0,
	    1.0, -1.0, -1.0,
	    1.0,  1.0, -1.0,
	    -1.0,  1.0, -1.0,
		-1.0, -1.0,  1.0,
	    1.0, -1.0,  1.0,
	    1.0,  1.0,  1.0,
	    -1.0,  1.0,  1.0,
	};
	indices = {
		//front
		0, 1, 2,
		2, 3, 0,
		// right
		1, 5, 6,
		6, 2, 1,
		// back
		7, 6, 5,
		5, 4, 7,
		// left
		4, 0, 3,
		3, 7, 4,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// top
		3, 2, 6,
		6, 7, 3,
	};
}

void Background::initializeBuffers()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);


	glGenBuffers(1, &pointsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(float), &points[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &textureCoordinatesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, textureCoordinatesBuffer);
	glBufferData(GL_ARRAY_BUFFER, textureCoordinates.size() * sizeof(float), &textureCoordinates[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &elementBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Background::setPosition(double x, double y, double z)
{
	position = glm::vec3(x, y, z);
}

void Background::setPosition(const glm::vec3 & position)
{
	this->position = position;
}

void Background::setTexture(GLuint texture)
{
	this->texture = texture;
}

GLuint Background::getTexture() const
{
	return texture;
}

void Background::update()
{
}
void Background::draw()
{
	shader.useProgram();
	glBindVertexArray(vao);

	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, textureCoordinatesBuffer);
	glEnableVertexAttribArray(1);

	glm::mat4 model(1.0f);
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.0)) * glm::scale(glm::mat4(1.0f), glm::vec3(5.5f, 5.5f, 5.5f));

	shader.setMat4("model", model);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);

	glDepthFunc(GL_LEQUAL);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, 0);
	glDepthFunc(GL_LESS);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}