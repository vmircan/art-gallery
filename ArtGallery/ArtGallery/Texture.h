#pragma once

#include <string>
struct Texture {
	Texture() = default;
	Texture(unsigned id, const std::string& type):
		id(id), type(type){}
	unsigned id;
	std::string type;
};