#version 330

in vec2 textureCoordinates;
in vec3 normal;
in vec3 pos;

out vec4 fragColor;

uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_diffuse2;
uniform sampler2D texture_diffuse3;
uniform sampler2D texture_specular1;
uniform sampler2D texture_specular2;

uniform float scaleFactor;

void main() {
	float ambientStrength = 0.1 * scaleFactor;
	vec3 ambient = lightColor * ambientStrength; 

	float dayDiffuseStrength = 0.8 * scaleFactor;
	float dotProduct = dot(lightPos, normal);
	float dayDiff = max(dotProduct, 0);
	vec3 dayDiffuse = dayDiffuseStrength * dayDiff * lightColor;

	float specularStrength = 5.0 * scaleFactor;
	vec3 lightDir = normalize(lightPos - pos);
	vec3 reflectDir = reflect(-lightDir, normal);
	vec3 viewDir = normalize(viewPos - pos);
	float spec = pow(max(dot(viewDir, reflectDir),0.0), 32);
	vec3 specular =  spec * lightColor * texture(texture_specular1, textureCoordinates).rgb * specularStrength;

	fragColor = texture(texture_diffuse1, textureCoordinates) * vec4(ambient + dayDiffuse, 1.0);
	//fragColor = vec4(0.5, 0.0, 0.0, 1.0) * vec4(ambient + dayDiffuse, 1.0);
	//fragColor = vec4(0.5, 0.0, 0.0, 1.0);
};