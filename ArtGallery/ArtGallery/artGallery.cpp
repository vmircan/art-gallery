#include "Background.h"
#include "OpenGLUtils.h"
#include "ShaderLoader.h"
#include "Scene.h"
#include "GlassSupport.h"
#include "Model.h"
#include "Camera2.h"

#include <iostream>


int main() {
	glDisable(GL_CULL_FACE);
	int width = 1000, height = 700;
	const float nearPlane = 0.1f, farPlane = 100.0f;
	GLFWwindow* window = OpenGLUtils::initializeWindow(width, height, "XO");

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	ShaderLoader glassSupportShaders("GlassSupport.vs", "GlassSupport.fs");

	ShaderLoader modelShaders("model.vs", "model.fs");


	GlassSupport glassSupport(glassSupportShaders);
	GlassSupport glassSupport2(glassSupportShaders);

	Model tolerance("..\\..\\Models\\tolerance-statue\\source\\statue.obj", modelShaders);
	Model venus("..\\..\\Models\\venus-de-milo\\source\\retopo_2.obj", modelShaders);
	Model discobolus("..\\..\\Models\\discobolus2\\source\\disco.obj", modelShaders);
	Model vase("..\\..\\Models\\vase-antique-grec\\source\\vase.obj", modelShaders);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetCursorPosCallback(window, Scene::mouseCallback);
	glfwSetMouseButtonCallback(window, Scene::mouseButtonCallback);
	glfwSetScrollCallback(window, Scene::mouseScrollCallback);
	glfwSetKeyCallback(window, Scene::keyboardCallback);

	ShaderLoader bs("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
	Background background(bs);


	Scene::addObject("venus", &venus);
	Scene::addObject("discobolus", &discobolus);
	Scene::addObject("vase", &vase);
	Scene::addObject("tolerance", &tolerance);
	Scene::addObject("cube", &glassSupport);
	Scene::addObject("cube2", &glassSupport2);

	Scene::initialize();
	unsigned toleranceDiffuseTexture = OpenGLUtils::generateTexture2D
	("..\\..\\Models\\tolerance-statue\\source\\socha_tolerance.jpg");
	unsigned toleranceSpecularTexture = OpenGLUtils::generateTexture2D
	("..\\..\\Models\\tolerance-statue\\source\\socha_toleranceNormalsMap.jpg");
	background.initialize();

	unsigned venusDiffuseTexture = OpenGLUtils::generateTexture2D
	("..\\..\\Models\\venus-de-milo\\source\\retopo_2_Default_ao.jpeg");

	unsigned venusSpecularTexture = OpenGLUtils::generateTexture2D
	("..\\..\\Models\\venus-de-milo\\source\\retopo_2_Default_nmap.jpeg");


	std::vector<std::string> faces = { "SkyBoxMuseum/right.jpg" , "SkyBoxMuseum/left.jpg", "SkyBoxMuseum/top.jpg",
	"SkyBoxMuseum/bottom.jpg", "SkyBoxMuseum/front.jpg", "SkyBoxMuseum/back.jpg", };
	unsigned skyBoxTexture = OpenGLUtils::generateTextureCubeMap(faces);

	background.setTexture(skyBoxTexture);

	Camera2 camera0(1.0f, -0.7f, 1.0f,
		0.0f, 1.0f, 0.0f,
		75.0f, -90.0f, 0.0f, 0.0f);
	Scene::addCamera(&camera0);
	camera0.setRotationCenter(0.0, -0.7, 0.0);

	Camera2 camera1(2.0f, -1.0f, 0.6f,
		0.0f, 1.0f, 0.0f,
		75.0f, -90.0f, 0.0f, 0.0f);
	Scene::addCamera(&camera1);
	camera1.setRotationCenter(2.0f, -1.0f, -1.0f);

	Camera2 camera2(1.6f, -0.6f, 0.4f,
		0.0f, 1.0f, 0.0f,
		75.0f, 100.0f, 0.0f, 0.0f);
	Scene::addCamera(&camera2);
	camera2.setRotationCenter(1.6f, -0.6f, 1.9f);

	Camera2 camera3(-1.15f, -0.3f, 0.2f,
		0.0f, 1.0f, 0.0f,
		75.0f, 140.0f, 0.0f, 0.0f);
	Scene::addCamera(&camera3);
	camera3.setRotationCenter(-2.0, -0.3, 0.5);

	Camera2 camera4(-0.7f, 0.1f, -0.2f,
		0.0f, 1.0f, 0.0f,
		75.0f, -100.0f, 0.0f, 0.0f);
	Scene::addCamera(&camera4);
	camera4.setRotationCenter(-1.05, 0.1, -1.56);

	vase.setPosition(-1.5f, 2.203f, -0.85f);
	float vaseScaleFactor = 15.0;
	vase.setScale(0.35f, 0.35f, 0.35f);
	vase.setScaleFactor(vaseScaleFactor);

	glm::dquat vaseQuat = glm::dquat(0.0, 0.0, 0.0, 1.0);
	vaseQuat = glm::dquat(glm::angleAxis(glm::radians(65.0f), glm::vec3(1.0f, 0.0f, 0.0f))) * vaseQuat;
	vaseQuat = glm::dquat(glm::angleAxis(glm::radians(155.0f), glm::vec3(0.0f, 1.0f, 0.0f))) * vaseQuat;
	vaseQuat = glm::dquat(glm::angleAxis(glm::radians(4.2f), glm::vec3(0.0f, 0.0f, 1.0f))) * vaseQuat;

	vase.setRotation(vaseQuat);


	discobolus.setPosition(-2.0f, -0.9f, 0.5f);
	float discobolusScaleFactor = 0.4f;
	discobolus.setScale(0.07f, 0.07f, 0.07f);
	discobolus.setScaleFactor(discobolusScaleFactor);

	glm::dquat discobolusQuat = glm::dquat(0.0, 0.0, 0.0, 1.0);
	discobolusQuat = glm::dquat(glm::angleAxis(glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f))) * discobolusQuat;
	discobolusQuat = glm::dquat(glm::angleAxis(glm::radians(155.0f), glm::vec3(0.0f, 1.0f, 0.0f))) * discobolusQuat;

	discobolus.setRotation(discobolusQuat);

	venus.setPosition(1.6f, -2.8f, 1.9f);
	float venusScaleFactor = 0.0025f;
	venus.setScale(0.0015f, 0.0015f, 0.0015f);
	venus.setScaleFactor(venusScaleFactor);

	glm::dquat venusQuat = glm::dquat(0.0, 0.0, 0.0, 1.0);
	venusQuat = glm::dquat(glm::angleAxis(glm::radians(-180.0f), glm::vec3(1.0f, 0.0f, 0.0f))) * venusQuat;
	venusQuat = glm::dquat(glm::angleAxis(glm::radians(-15.0f), glm::vec3(0.0f, 1.0f, 0.0f))) * venusQuat;
	venusQuat = glm::dquat(glm::angleAxis(glm::radians(5.0f), glm::vec3(0.0f, 0.0f, 1.0f))) * venusQuat;

	venus.setRotation(venusQuat);

	venus.addDiffuseTexture(venusDiffuseTexture);
	venus.addSpecularTexture(venusSpecularTexture);

	tolerance.setPosition(2.0f, -2.8f, -1.0f);
	float toleranceScaleFactor = 0.7f;
	tolerance.setScale(0.7f, 0.7f, 0.7f);
	tolerance.setScaleFactor(toleranceScaleFactor);

	glm::dquat toleranceQuat = glm::dquat(0.0, 0.0, 0.0, 1.0);
	toleranceQuat = glm::dquat(glm::angleAxis(glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f))) * toleranceQuat;
	toleranceQuat = glm::dquat(glm::angleAxis(glm::radians(85.0f), glm::vec3(0.0f, 1.0f, 0.0f))) * toleranceQuat;
	toleranceQuat = glm::dquat(glm::angleAxis(glm::radians(5.0f), glm::vec3(0.0f, 0.0f, 1.0f))) * toleranceQuat;

	tolerance.setRotation(toleranceQuat);

	tolerance.addDiffuseTexture(toleranceDiffuseTexture);
	tolerance.addSpecularTexture(toleranceSpecularTexture);

	glassSupport.setPosition(-2.0f, -1.8f, 0.5f);
	glassSupport.setScale(0.4, 1.0, 0.2);
	glassSupport.setTexture(skyBoxTexture);
	
	glassSupport2.setPosition(-1.0f, -1.8f, -1.6f);
	glassSupport2.setScale(0.4, 1.0, 0.4);
	glassSupport2.setTexture(skyBoxTexture);

	modelShaders.useProgram();

	modelShaders.setVec3("lightPos", glm::vec3(0.0f, 1.0f, 0.0f));
	modelShaders.setVec3("lightColor", glm::vec3(1.0f, 1.0f, 1.0f));

	do
	{
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glfwGetWindowSize(window, &width, &height);
		glfwSetCursorPos(window, width / 2, height / 2);
		Scene::getActiveCamera()->centerX = width / 2.0f;
		Scene::getActiveCamera()->centerY = height / 2.0f;
		glm::mat4 projection = glm::perspective(glm::radians(Scene::getActiveCamera()->getFov()), (float)width / height,
			nearPlane, farPlane);

		glm::mat4 view = Scene::getActiveCamera()->getViewMatrix();

		modelShaders.useProgram();

		modelShaders.setMat4("projection", projection);
		modelShaders.setMat4("view", view);

		modelShaders.setVec3("viewPos", Scene::getActiveCamera()->getPosition());

		glassSupportShaders.useProgram();

		glassSupportShaders.setMat4("projection", projection);
		glassSupportShaders.setMat4("view", view);

		glassSupportShaders.setVec3("viewPos", Scene::getActiveCamera()->getPosition());
		glassSupportShaders.setFloat("fov", Scene::getActiveCamera()->getFov());

		Scene::draw();

		bs.useProgram();

		bs.setMat4("projection", projection);
		bs.setMat4("view", view);

		background.setPosition(Scene::getActiveCamera()->getPosition());
		background.draw();


		glfwSwapBuffers(window);
		glfwPollEvents();
	} while (!glfwWindowShouldClose(window) && glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS);
	glfwTerminate();
	return 0;
}