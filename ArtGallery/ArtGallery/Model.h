#pragma once
#include "Mesh.h"
#include "OpenGLObject.h"
#include <assimp/scene.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <gtx\quaternion.hpp>

class Model : public OpenGLObject
{
public:
	Model(const std::string& path, ShaderLoader shader);
	void draw();
	void initialize();
	void update();
	void setPosition(const glm::vec3& position);
	void setPosition(float x, float y, float z);
	void setScale(const glm::vec3& scale);
	void setScale(float xDimension, float yDimension, float zDimension);
	void setRotation(const glm::dquat& quat);
	glm::dquat quat = glm::dquat(0.0, 0.0, 0.0, 1.0);
	void setScaleFactor(float scaleFactor);
	float getScaleFactor() const;
	ShaderLoader& getShader();
	void addDiffuseTexture(unsigned diffuseTexture);
	void addSpecularTexture(unsigned specularTexture);
private:
	ShaderLoader shader;
	std::vector<Mesh> meshes;
	std::string directory;
	void loadModel(const std::string& path);
	void processNode(aiNode *node, const aiScene *scene);
	Mesh processMesh(aiMesh *mesh, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type,
		std::string typeName);
	glm::vec3 position, scale;
	float scaleFactor;
};