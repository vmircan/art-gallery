#pragma once
#include "glm.hpp"
#include <GL\glew.h>
#include <glfw3.h>
class Camera2
{
public:
	Camera2(float x, float y, float z, float upX,
		float upY, float upZ, float fov, float yaw, float pitch, float roll);
	~Camera2();

	void refreshVectors();
	void updateZoomLevel(double yZoom);
	void processMouseMovement(double x, double y);
	void processKeyboardInput(int key, int action);
	const glm::vec3& getPosition() const;
	const glm::vec3& getFront() const;
	const glm::vec3& getUp() const;
	const glm::mat4 getViewMatrix() const;
	void resetCamera();
	void toggleMouseMovement();
	float getFov() const;
	float getRotationRadius() const;
	void setRotationRadius(float rotationRadius);
	void setRotationCenter(const glm::vec3& rotationCenter);
	void setRotationCenter(double x, double y, double z);
	void setMouseMovementEnabled(bool enabled);
	bool getMouseMovementEnabled() const;

public:
	float centerX, centerY;

private:
	bool firstMouse = true;
	bool mouseMovementEnabled = false;
	bool rotationEnabled = false;
	float rotationRadius;
	glm::vec3 rotationCenter = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 rotationStartingPosition = glm::vec3(0.0, 0.0, 0.0);
	float fov;
	float maxFov = 120.0f, minFov = 1.0f;
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	float yaw, pitch, roll;
	float initialYaw, initialPitch, initialRoll;
	const glm::vec3 initialPosition, initialUp;
	const float initialFov;
	const float mouseSensitivity = 0.5f, mouseScrollSensitivity = 1.0f;
	const float keySensitivity = 0.05f;
};

