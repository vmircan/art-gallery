#version 330
#extension GL_NV_shadow_samplers_cube : enable

in vec3 normal;
in vec3 pos;

out vec4 fragColor;

uniform vec3 viewPos;
uniform float fov;

uniform samplerCube texture;


void main() {
	vec3 viewPos2 = viewPos / (fov/45.0);
	float refractiveIndex = 1.0 / 1.1;
	vec3 incidence = normalize(pos - viewPos2);
	vec3 reflect = refract(incidence, normalize(normal), refractiveIndex);
	fragColor = vec4(textureCube(texture, reflect).rgb, 1.0);
};