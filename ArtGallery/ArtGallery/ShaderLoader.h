#pragma once
#include <string>
#include <GL\glew.h>
#include <glm.hpp>
class ShaderLoader
{
public:
	ShaderLoader(const std::string& vertexPath, const std::string& fragmentPath);
	~ShaderLoader();
	bool loadShaders();
	GLuint vertexShaderId, fragmentShaderId;
	void setInt(const std::string& name, int value);
	void setFloat(const std::string& name, float value);
	void setVec3(const std::string& name,  const glm::vec3& value);
	void setMat4(const std::string& name, const glm::mat4& value);
	void useProgram();
private:
	const std::string vertexShaderPath, fragmentShaderPath;
	GLuint programId;
};

