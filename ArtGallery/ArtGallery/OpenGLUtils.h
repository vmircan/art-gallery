#pragma once

#include <string>
#include <GL\glew.h>
#include <glfw3.h>
#include <vector>


namespace OpenGLUtils {
	GLFWwindow* initializeWindow(unsigned width, unsigned height, const std::string& title);
	GLuint generateTexture2D(const std::string& name, const std::string& directory);
	GLuint generateTexture2D(const std::string& path);
	GLuint generateTextureCubeMap(const std::vector<std::string>& faces);
	void processInput(GLFWwindow* window);
	void flipTexturesVertically(bool flip);
}

