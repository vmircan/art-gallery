#pragma once
#include "OpenGLObject.h"
#include "OpenGLUtils.h"
#include "ShaderLoader.h"
#include <string>
#include <vector>
#include <gtc/quaternion.hpp>

#include "glm.hpp"

class GlassSupport : public OpenGLObject
{
public:
	GlassSupport(ShaderLoader shader);
	~GlassSupport();

	void initialize() override;
	void initializeVertices();
	void initializeBuffers();
	void update() override;
	void draw() override;
	void setTexture(GLuint texture);
	void setPosition(const glm::vec3& position);
	void setPosition(double x, double y, double z);
	void setScale(const glm::vec3& scale);
	void setScale(double xDimension, double yDimension, double zDimension);
	void setRotation(const glm::dquat& quat);
private:
	ShaderLoader shader;

	GLuint texture;

	GLuint vao, pointsBuffer, normalsBuffer, textureCoordinatesBuffer;
	glm::dquat quat;
	std::vector<float> points;
	std::vector<float> normals;
	std::vector<float> textureCoordinates;

	glm::vec3 position, scale;
};

