#pragma once
#include "OpenGLObject.h"
#include "OpenGLUtils.h"
#include "ShaderLoader.h"
#include <string>
#include <vector>

#include "glm.hpp"

class Background : public OpenGLObject
{
public:
	Background(ShaderLoader shader);
	~Background();

	void initialize() override;
	void initializeVertices();
	void initializeBuffers();
	void setPosition(double x, double y, double z);
	void setPosition(const glm::vec3& position);
	void setTexture(GLuint texture);
	GLuint getTexture() const;
	void update() override;
	void draw() override;

private:
	ShaderLoader shader;

	GLuint texture;
	GLuint vao, pointsBuffer, textureCoordinatesBuffer, elementBuffer;

	std::vector<float> points;
	std::vector<float> textureCoordinates;
	std::vector<unsigned short> indices;

	glm::vec3 position= glm::vec3(0.0f, 0.0f, 0.0f);
};

